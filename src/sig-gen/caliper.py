import argparse
import ssl
import OpenSSL
import hashlib
import os
import subprocess
import json

def main():
    opts = get_opts()
    # print(opts.caliper_dir)
    cert_dir = os.path.join(opts.caliper_dir, 'certs')
    rules = []

    cert_paths = []

    # allow one level of subdirectories
    for f in os.listdir(cert_dir):
        f_file = os.path.join(cert_dir, f)
        if os.path.isfile(f_file):
            cert_paths.append(f_file)
        else:
            for f2 in os.listdir(f_file):
                f2_file = os.path.join(f_file, f2)
                if os.path.isfile(f2_file):
                    cert_paths.append(f2_file)

    for pem_path in cert_paths:
        with open(pem_path, 'r') as pem_file:
            pem_text = pem_file.read()
        fp = pem_to_fingerprint(pem_text)

        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, pem_text)

        for i in range(x509.get_extension_count()):
            ext = x509.get_extension(i)
            if ext.get_short_name() == b'subjectAltName':
                san = str(ext)
                break

        domain_rule = ','.join([domain.replace('DNS:', '').strip() for domain in san.split(',')])
        rules.append(f"{domain_rule} {fp}")

    rules.sort()
    rules = '\n'.join(rules)

    rules_path = os.path.join(opts.caliper_dir, 'rules.txt')
    with open(rules_path, 'w') as rules_file:
        rules_file.write(rules)

    priv_key = os.path.join(opts.caliper_dir, 'secret.key')
    ran = subprocess.run(['minisign', '-q', '-S', '-s', f'{priv_key}', '-m', f"{rules_path}"], capture_output=True, input=b"\n\n")

    if ran.returncode != 0:
        print('!!', ran.stdout.decode(), '!!')
        print('!!', ran.stderr.decode(), '!!')
        return

    with open(f"{rules_path}.minisig", 'r') as sig_file:
        rules_sig = sig_file.read()

    caliper_data = {
        "version": 2,
        "rules": rules,
        "signature": rules_sig
    }

    caliper_data_path = os.path.join(opts.caliper_dir, 'caliper.json')

    with open(caliper_data_path, 'w') as caliper_file:
        caliper_file.write(json.dumps(caliper_data, indent=2))

def pem_to_fingerprint(pem_text):
    cert = ssl.PEM_cert_to_DER_cert(pem_text)
    fp = hashlib.sha256(cert).hexdigest().upper()
    fp = ':'.join(fp[pos:pos+2] for pos in range(0,len(fp),2))
    return fp

def get_opts():
    parser = argparse.ArgumentParser(epilog=HELP_EPILOG, description=HELP_DESCRIPTION, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('caliper_dir', help="The caliper dir path. See STRUCTURE for information on is structure", metavar="<caliper_dir>")
    return parser.parse_args()

HELP_DESCRIPTION = """
CAliper's JSON creator and signer helper.
"""
HELP_EPILOG = """
This script takes a directory structure of certs and creates a signature file that can be served to the CAliper extension.
The subdirectory names can be anything. The domains in use are take from the Subject Alternative Name (SAN) of each certificate.
There should be a Minisign private key named "secret.key" at the top level of this directory.
This should be run on a trusted/secure host.

STRUCTURE
The structure CAliper expects is like so:

caliper/
├── certs/
│   ├── certA.crt
│   ├── certB.pem
│   ├── caliper.example.com/
│   │   ├── cert1.pem
│   │   └── cert2.crt
│   ├── *.example.com/
│   │   └── cert.pem
│   └── example.com,www.example.com/
│       └── cert.pem
└── secret.key
"""

if __name__ == "__main__":
    main()