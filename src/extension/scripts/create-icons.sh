#!/usr/bin/env bash
set -e

# CWD is folder with package.json in it

mkdir -p src/icons

# create rounded masks
gm convert -size 96x96 xc:none -draw "roundRectangle 0,0 95,95 8,8" src/icons/mask96.png
gm convert -size 38x38 xc:none -draw "roundRectangle 0,0 37,37 6,6" src/icons/mask38.png

# default icon
gm convert -resize 96x96 -background white caliper.svg 'src/icons/caliper@2x.png'
gm composite -compose copyopacity src/icons/mask96.png 'src/icons/caliper@2x.png' 'src/icons/caliper@2x.png'
gm convert -resize 48x48 'src/icons/caliper@2x.png' src/icons/caliper.png

# green page action icon
gm convert -resize 38x38 -background '#2f2' caliper.svg src/icons/mask38.png 'src/icons/caliper-green@2x.png'
gm composite -compose copyopacity src/icons/mask38.png 'src/icons/caliper-green@2x.png' 'src/icons/caliper-green@2x.png'
gm convert -resize 19x19 'src/icons/caliper-green@2x.png' src/icons/caliper-green.png

# yellow page action icon
gm convert -resize 38x38 -background '#fe2' caliper.svg src/icons/mask38.png 'src/icons/caliper-yellow@2x.png'
gm composite -compose copyopacity src/icons/mask38.png 'src/icons/caliper-yellow@2x.png' 'src/icons/caliper-yellow@2x.png'
gm convert -resize 19x19 'src/icons/caliper-yellow@2x.png' src/icons/caliper-yellow.png

# clean up masks
rm src/icons/mask96.png src/icons/mask38.png
