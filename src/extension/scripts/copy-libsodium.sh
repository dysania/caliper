#!/usr/bin/env bash
set -e

# CWD is folder with package.json in it

# NOTE: It would be better to use symlinks for these, but the web-ext tool
# doesn't recognize them during linting. If it ever does then this should be
# changed.
cp node_modules/libsodium/dist/modules/libsodium.js src/
cp node_modules/libsodium-wrappers/dist/modules/libsodium-wrappers.js src/
