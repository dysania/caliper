(() => {
  const defaultCaliperOptions = {
    publicKey: '',
    jsonUrl: '',
    fingerprints: {},
    autoUpdate: true
  };

  window.CAliper = {
    sodium: new Promise((resolve) => {
      window.sodium = {
        onload: resolve
      };
    }),
    getOptions() {
      return browser.storage.local.get(defaultCaliperOptions);
    },
    setOptions(options) {
      const newOptions = {};

      for (let key of Object.keys(defaultCaliperOptions)) {
        if (key in options) {
          newOptions[key] = options[key];
        }
      }

      return browser.storage.local.set(newOptions);
    },
    decodeFingerprints(text) {
      const fingerprints = {};

      for (let line of text.trim().split('\n')) {
        if (line) {
          let [domains, fingerprint] = line.split(' ');
          domains = domains.split(',');

          fingerprints[fingerprint] = domains;
        }
      }

      return fingerprints;
    },
    encodeFingerprints(fingerprints) {
      const lines = [];

      for (let [fingerprint, domains] of Object.entries(fingerprints)) {
        lines.push(`${domains.join(',')} ${fingerprint}`);
      }

      return lines.join('\n');
    },
    nextTick() {
      return new Promise((resolve) => {
        setTimeout(resolve, 0);
      });
    },
    isBrowserCompatible() {
      return typeof browser.webRequest.getSecurityInfo === 'function';
    }
  };
})();

window.CAliper.ready = (async (CAliper) => {
  // Wait one tick to give MinisignVerify a chance to populate on window
  await CAliper.nextTick();

  const { verifySignature } = await window.MinisignVerify;
  let updatePromise = null;
  const _updateFingerprintsFromServer = async () => {
    const { publicKey, jsonUrl, fingerprints } = await CAliper.getOptions();

    if (!jsonUrl) {
      throw new Error('JSON URL missing');
    }

    if (!publicKey) {
      throw new Error('Public key missing');
    }

    const response = await fetch(jsonUrl).then((res) => res.json());

    if (response.version !== 1) {
      throw new Error('Unknown version: ' + JSON.stringify(response));
    }

    const [validSignature, err] = verifySignature(
      publicKey,
      response.fingerprints,
      response.signature
    );

    if (!validSignature) {
      throw err;
    }

    const newFingerprints = CAliper.decodeFingerprints(response.fingerprints);

    for (let [fingerprint, domains] of Object.entries(newFingerprints)) {
      if (fingerprint in fingerprints) {
        // update domains by creating a set over new and old domains
        fingerprints[fingerprint] = [
          ...new Set([...fingerprints[fingerprint], ...domains])
        ];
      } else {
        fingerprints[fingerprint] = domains;
      }
    }

    return CAliper.setOptions({ fingerprints });
  };

  Object.assign(CAliper, {
    // Wraps the update function to only allow one server request at a time
    updateFingerprintsFromServer() {
      if (updatePromise == null) {
        updatePromise = _updateFingerprintsFromServer()
          .then(() => {
            updatePromise = null;
          })
          .catch((e) => {
            updatePromise = null;
            throw e;
          });
      }

      return updatePromise;
    }
  });
})(window.CAliper);
