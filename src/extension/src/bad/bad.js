const link = document.getElementById('url');
const moreInfoTBody = document.querySelector('#more-info tbody');
const openOptions = document.getElementById('open-options');
const showIp = document.getElementById('data-ip');
const showDataFingerprint = document.getElementById('data-browser-fingerprint');
const certValidStart = document.getElementById('cert-valid-start');
const certValidEnd = document.getElementById('cert-valid-end');
const matchedFingerprint = document.getElementById('data-matched-fingerprint');
const matchedFingerprintTemplate = document.getElementById(
  'matched-fingerprint-tr'
);
const data = JSON.parse(new URL(location).searchParams.get('data'));
const certificate = data.securityInfo.certificates[0];
const browserFingerprint = certificate.fingerprint.sha256;

link.textContent = data.url;
link.href = data.url;

openOptions.addEventListener('click', () => {
  browser.runtime.openOptionsPage();
});

showIp.textContent = data.ip;
showDataFingerprint.textContent = browserFingerprint;
certValidStart.textContent = new Date(certificate.validity.start).toLocaleString();
certValidEnd.textContent = new Date(certificate.validity.end).toLocaleString();

matchedFingerprint.textContent = data.matchedFingerprints[0];

for (let i = 1; i < data.matchedFingerprints.length; i++) {
  const clone = matchedFingerprintTemplate.content.cloneNode(true);
  clone.querySelector('td:last-of-type').textContent =
    data.matchedFingerprints[i];
  moreInfoTBody.appendChild(clone);
}
