(async (CAliper) => {
  await CAliper.ready;

  const { getOptions, updateFingerprintsFromServer, isBrowserCompatible } =
    CAliper;
  const { version } = browser.runtime.getManifest();

  console.log(`CAliper v${version} started at`, new Date().toLocaleString());

  if (isBrowserCompatible()) {
    initWebRequstListener();

    browser.storage.onChanged.addListener((changes, areaName) => {
      if (areaName === 'local') {
        initWebRequstListener();
      }
    });

    setInterval(() => {
      getOptions()
        .then(({ autoUpdate }) => {
          if (autoUpdate) {
            return updateFingerprintsFromServer();
          }
        })
        .catch((err) => {
          console.log('Update error:', err);
        });
    }, 30 * 60 * 1000);
  } else {
    console.log('This extension is not compatible with this browser');
  }

  function initWebRequstListener() {
    getMonitoredUrls()
      .then((urls) => {
        console.log('Monitoring:\n' + urls.join('\n'));

        browser.webRequest.onHeadersReceived.removeListener(caliper);

        if (urls.length > 0) {
          browser.webRequest.onHeadersReceived.addListener(caliper, { urls }, [
            'blocking'
          ]);
        }
      })
      .catch((e) => {
        console.log('Failed to get urls for webRequest listener');
        console.log(e);
      });
  }

  async function caliper(webRequest) {
    const { hostname } = new URL(webRequest.url);

    try {
      const secInfo = await browser.webRequest.getSecurityInfo(
        webRequest.requestId,
        {}
      );
      const certificate = secInfo.certificates[0];
      const browserFingerprint = certificate.fingerprint.sha256;

      let matchedFingerprints = await getMatchedFingerprints(hostname);
      let isValid = matchedFingerprints.some(
        ([fingerprint]) => fingerprint === browserFingerprint
      );

      // When first seeing an invalid request, update known fingerprints and
      // check again
      if (!isValid) {
        console.log(
          'Double checking request',
          webRequest,
          secInfo,
          browserFingerprint,
          matchedFingerprints
        );

        // Start the request;
        const serverUpdate = updateFingerprintsFromServer();

        // Indicate an update is pending with icon
        await browser.pageAction.show(webRequest.tabId);
        await browser.pageAction.setIcon({
          tabId: webRequest.tabId,
          path: 'icons/caliper-yellow@2x.png'
        });
        await browser.pageAction.setTitle({
          tabId: webRequest.tabId,
          title: 'A request is pending'
        });

        // Wait for request and check again
        await serverUpdate;

        matchedFingerprints = await getMatchedFingerprints(hostname);
        isValid = matchedFingerprints.some(
          ([fingerprint]) => fingerprint === browserFingerprint
        );
      }

      // If still invalid after the update then show warning page
      if (!isValid) {
        console.log(
          'Error with request',
          webRequest,
          secInfo,
          browserFingerprint,
          matchedFingerprints
        );

        const data = {
          url: webRequest.url,
          ip: webRequest.ip,
          securityInfo: secInfo,
          matchedFingerprints: matchedFingerprints.map(
            ([fingerprint]) => fingerprint
          )
        };

        await browser.tabs.update(webRequest.tabId, {
          url:
            browser.runtime.getURL('bad/bad.html') +
            '?data=' +
            encodeURIComponent(JSON.stringify(data))
        });

        return;
      }

      // If valid then show green icon
      await browser.pageAction.show(webRequest.tabId);
      await browser.pageAction.setIcon({
        tabId: webRequest.tabId,
        path: 'icons/caliper-green@2x.png'
      });
      await browser.pageAction.setTitle({
        tabId: webRequest.tabId,
        title: 'All requests validated'
      });
    } catch (error) {
      console.log(error);
    }
  }

  async function getMatchedFingerprints(hostname) {
    const { fingerprints } = await getOptions();
    const globMatchHostname = globMatchDomain.bind(null, hostname);
    return Object.entries(fingerprints).filter(([, domains]) => {
      return domains.some((globDomain) => globMatchHostname(globDomain));
    });
  }

  async function getMonitoredUrls() {
    const { fingerprints } = await getOptions();
    const urls = new Set();

    for (let domains of Object.values(fingerprints)) {
      for (let domain of domains) {
        urls.add(`https://${domain}/*`);
      }
    }

    return [...urls];
  }

  function globMatchDomain(hostname, globDomain) {
    // turn glob "." into "\." and "*." into "(.+\.)?"
    const domainRegex = new RegExp(
      `^${globDomain.replace(/\./g, '\\.').replace(/^\*\\\./, '(.+\\.)?')}$`,
      'g'
    );

    return domainRegex.test(hostname);
  }
})(window.CAliper);
