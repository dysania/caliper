(async (CAliper) => {
  await CAliper.ready;

  const BASE64_PUBLICKEY_LENGTH = 56;
  const PUBLIC_KEY_REGEX = new RegExp(
    `^(untrusted comment: .*\n)?[A-Za-z0-9/+=]{${BASE64_PUBLICKEY_LENGTH}}$`
  );
  const GLOB_SINGLE_DOMAINNAME =
    '(\\*\\.)?[a-z0-9]+(-[a-z0-9]+)*(\\.[a-z0-9]+(-[a-z0-9]+)*)*';
  const GLOB_DOMAINNAME_REGEX = new RegExp(
    `^(${GLOB_SINGLE_DOMAINNAME},)*${GLOB_SINGLE_DOMAINNAME} ([A-Z0-9]{2}:){31}[A-Z0-9]{2}$`
  );
  const browserIncompatibleEl = document.getElementById('browser-incompatible');
  const publicKeyEl = document.getElementById('public-key');
  const jsonUrlEl = document.getElementById('json-url');
  const fingerprintsEl = document.getElementById('fingerprints');
  const autoUpdateEl = document.getElementById('auto-update');
  const getFingerprintsEl = document.getElementById('get-fingerprints');
  const serverStatusEl = document.getElementById('server-status');
  let serverStatusTimeout = null;

  const saveOptions = debounce(async () => {
    const options = {
      autoUpdate: autoUpdateEl.checked
    };

    if (publicKeyEl.checkValidity()) {
      options.publicKey = publicKeyEl.value;
    }

    if (jsonUrlEl.checkValidity()) {
      options.jsonUrl = jsonUrlEl.value;
    }

    if (fingerprintsEl.checkValidity()) {
      options.fingerprints = CAliper.decodeFingerprints(fingerprintsEl.value);
    }

    await CAliper.setOptions(options);
  }, 300);

  if (CAliper.isBrowserCompatible()) {
    restoreOptions().then(validateJsonUrl);
  } else {
    browserIncompatibleEl.classList.remove('hidden');

    for (let el of document.querySelectorAll('.option')) {
      el.classList.add('hidden');
    }
  }

  publicKeyEl.addEventListener('input', validatePublicKey);
  publicKeyEl.addEventListener('input', saveOptions);
  jsonUrlEl.addEventListener('input', debounce(validateJsonUrl, 300));
  jsonUrlEl.addEventListener('input', saveOptions);
  fingerprintsEl.addEventListener('input', validateFingerprints);
  fingerprintsEl.addEventListener('input', saveOptions);
  getFingerprintsEl.addEventListener('click', getFingerprints);
  autoUpdateEl.addEventListener('change', saveOptions);

  async function restoreOptions() {
    const {
      publicKey,
      jsonUrl,
      fingerprints,
      autoUpdate
    } = await CAliper.getOptions();

    publicKeyEl.value = publicKey;
    jsonUrlEl.value = jsonUrl;
    fingerprintsEl.value = CAliper.encodeFingerprints(fingerprints);
    autoUpdateEl.checked = !!autoUpdate;
  }

  function getFingerprints() {
    if (getFingerprintsEl.disabled) {
      return;
    }

    getFingerprintsEl.disabled = true;
    serverStatusEl.textContent = 'Loading...';

    if (serverStatusTimeout != null) {
      clearTimeout(serverStatusTimeout);
    }

    CAliper.updateFingerprintsFromServer()
      .then(async () => {
        serverStatusEl.textContent = 'OK';

        await restoreOptions();
        validatePublicKey();
        validateFingerprints();
      })
      .catch((err) => {
        serverStatusEl.textContent = err;
      })
      .then(() => {
        getFingerprintsEl.disabled = false;

        serverStatusTimeout = setTimeout(() => {
          serverStatusEl.textContent = '';
        }, 3000);
      });
  }

  function validatePublicKey() {
    const value = publicKeyEl.value.trim();
    const match = value.match(PUBLIC_KEY_REGEX);

    if (!value || match) {
      publicKeyEl.setCustomValidity('');
    } else {
      publicKeyEl.setCustomValidity('Invalid public key format');
    }
  }

  function validateJsonUrl() {
    const jsonUrl = jsonUrlEl.value;

    if (!jsonUrl) {
      getFingerprintsEl.disabled = true;
      return;
    }

    try {
      // throws on error
      new URL(jsonUrl);
      getFingerprintsEl.disabled = false;
      jsonUrlEl.setCustomValidity('');
    } catch (err) {
      getFingerprintsEl.disabled = true;
      jsonUrlEl.setCustomValidity('Invalid JSON URL');
    }
  }

  function validateFingerprints() {
    const value = fingerprintsEl.value.trim();
    const match = value
      .split('\n')
      .every((line) => !line || line.match(GLOB_DOMAINNAME_REGEX));

    if (!value || match) {
      fingerprintsEl.setCustomValidity('');
    } else {
      fingerprintsEl.setCustomValidity('Invalid fingerprints format');
    }
  }

  function debounce(fn, wait) {
    let timeout = null;

    function run(...args) {
      if (timeout) {
        clearTimeout(timeout);
      }

      timeout = setTimeout(() => {
        fn(...args);
        timeout = null;
      }, wait);
    }

    return run;
  }
})(window.CAliper);
