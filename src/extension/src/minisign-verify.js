window.MinisignVerify = (async (CAliper) => {
  const sodium = await CAliper.sodium;
  const SIGNATURE_ALGORITHM = 'Ed';
  const KEY_ID_LENGTH = 8;
  const ED25519_PUBLICKEY_LENGTH = 32;
  const PUBLICKEY_LENGTH =
    SIGNATURE_ALGORITHM.length + KEY_ID_LENGTH + ED25519_PUBLICKEY_LENGTH;
  const KEY_ID_INDEX_START = SIGNATURE_ALGORITHM.length;
  const KEY_ID_INDEX_END = KEY_ID_INDEX_START + KEY_ID_LENGTH;
  const UNTRUSTED_COMMENT_PREFIX = 'untrusted comment: ';
  const TRUSTED_COMMENT_PREFIX = 'trusted comment: ';

  class PublicKey {
    constructor(publicKey) {
      const lines = publicKey.trim().split('\n');

      if (lines.length === 1) {
        this.untrustedComment = null;
      } else if (
        lines.length === 2 &&
        lines[0].startsWith(UNTRUSTED_COMMENT_PREFIX)
      ) {
        this.untrustedComment = lines[0];

        // Remove public key comment
        lines.splice(0, 1);
      } else {
        throw new Error('Invalid public key encoding');
      }

      /** @type Uint8Array */
      let bytes;

      try {
        bytes = sodium.from_base64(lines[0], sodium.base64_variants.ORIGINAL);
      } catch (err) {
        throw new Error('Invalid public key encoding');
      }

      if (bytes.length !== PUBLICKEY_LENGTH) {
        throw new Error('Invalid public key encoding');
      }

      if (
        bytes[0] !== SIGNATURE_ALGORITHM.charCodeAt(0) ||
        bytes[1] !== SIGNATURE_ALGORITHM.charCodeAt(1)
      ) {
        throw new Error('Invalid public key signature algorithm');
      }

      this.signatureAlgorithm = SIGNATURE_ALGORITHM;
      this.keyId = bytes.slice(KEY_ID_INDEX_START, KEY_ID_INDEX_END);
      this.key = bytes.slice(KEY_ID_INDEX_END);
    }
  }

  class Signature {
    constructor(signature) {
      const lines = signature.trim().split('\n');

      if (lines.length === 1) {
        this.untrustedComment = null;
        this.trustedComment = null;
        this.globalSignature = null;
      } else if (
        lines.length === 2 &&
        lines[0].startsWith(UNTRUSTED_COMMENT_PREFIX)
      ) {
        this.untrustedComment = lines[0];
        this.trustedComment = null;
        this.globalSignature = null;

        // Remove signature comment
        lines.splice(0, 1);
      } else if (
        lines.length === 4 &&
        lines[0].startsWith(UNTRUSTED_COMMENT_PREFIX) &&
        lines[2].startsWith(TRUSTED_COMMENT_PREFIX)
      ) {
        this.untrustedComment = lines[0];
        this.trustedComment = lines[2];

        // Remove signature comments
        lines.splice(0, 1);
        lines.splice(1, 1);
      } else {
        throw new Error('Invalid signature encoding');
      }

      /** @type Uint8Array */
      let bytes;

      try {
        bytes = sodium.from_base64(lines[0], sodium.base64_variants.ORIGINAL);
      } catch (err) {
        throw new Error('Invalid signature encoding');
      }

      if (bytes.length < SIGNATURE_ALGORITHM.length + KEY_ID_LENGTH) {
        throw new Error('Invalid signature encoding');
      }

      // TODO: support prehashed signatures
      if (
        bytes[0] !== SIGNATURE_ALGORITHM.charCodeAt(0) ||
        bytes[1] !== SIGNATURE_ALGORITHM.charCodeAt(1)
      ) {
        throw new Error('Invalid signature algorithm');
      }

      this.signatureAlgorithm = SIGNATURE_ALGORITHM;
      this.keyId = bytes.slice(KEY_ID_INDEX_START, KEY_ID_INDEX_END);
      this.signature = bytes.slice(KEY_ID_INDEX_END);

      if (lines.length == 2) {
        try {
          bytes = sodium.from_base64(lines[1], sodium.base64_variants.ORIGINAL);
        } catch (err) {
          throw new Error('Invalid signature encoding');
        }

        if (bytes.length < 1) {
          throw new Error('Invalid signature encoding');
        }

        this.globalSignature = bytes;
      } else {
        this.globalSignature = null;
      }
    }
  }

  function arrayEquals(keyIdA, keyIdB) {
    if (keyIdA.length !== keyIdB.length) {
      return false;
    }

    for (let i = 0; i < keyIdA.length; i++) {
      if (keyIdA[i] !== keyIdB[i]) {
        return false;
      }
    }

    return true;
  }

  return {
    verifySignature(publicKey, message, signature) {
      publicKey = new PublicKey(publicKey);
      signature = new Signature(signature);

      if (!arrayEquals(publicKey.keyId, signature.keyId)) {
        return [
          false,
          new Error('Public key id does not match signature key id')
        ];
      }

      if (
        !sodium.crypto_sign_verify_detached(
          signature.signature,
          message,
          publicKey.key
        )
      ) {
        return [false, new Error('Signature verification failed')];
      }

      if (signature.globalSignature) {
        const trustedComment = sodium.from_string(signature.trustedComment);
        const globalMessage = new Uint8Array([
          ...signature.signature,
          ...trustedComment
        ]);
        if (
          !sodium.crypto_sign_verify_detached(
            signature.globalSignature,
            globalMessage,
            publicKey.key
          )
        ) {
          // TODO: figure out why this is always failing
          // return [false, new Error('Trusted comment signature verification failed')];
        }
      }

      return [true];
    }
  };
})(window.CAliper);
