#!/usr/bin/env bash

default_minisign_secret="$HOME/.minisign/minisign.key"
default_certs_dir='./certs'
default_output_file='/dev/stdout'

usage() {
  cat <<EOL
Usage: $0 [--secret-key=FILE] [--certs=DIR] [--ouput=FILE]
       $0 --help

Options:
  -h, --help         show this message
  -c, --certs        certificates directory. Default $default_certs_dir
  -s, --secret-key   minisign secret key file. Default $default_minisign_secret
  -o, --output       output file. Default $default_output_file

Requirements:
  minisign
  python3

Structure:
  The certificates directory should have all public certificate files directly inside or in one
  level of subdirectories.
  The supported domains will be pulled from the Subject Alternative Name (SAN) of each certificate.

  certs/
  ├── cert1.cert
  ├── folder/
  │   └── cert2.cert
  └── cert3.pem
EOL
}

generate-data() {
  local certs_dir="$1"
  local data_file="$2"

  touch "$data_file"

  for cert in $(find "$certs_dir" -maxdepth 2 -name '*.crt' -o -name '*.pem'); do
    # echo $cert

    all_domains=`openssl x509 -noout -in "$cert" -text | grep DNS: | tr -d ' ' | tr , '\n' | sed 's/^DNS://g' | sort | paste -sd ','`
    fingerprint=`openssl x509 -noout  -in "$cert" -fingerprint -sha256 | cut -f2 -d=`
    echo "$all_domains $fingerprint" >> "$data_file"
  done

  sort -o "$data_file" "$data_file"
}

# correctly escape and quote contents of a file as a JSON string
jsonify() {
  local file="$1"

  python3 <<EOL
import json
import sys

with open('$file', 'r') as file:
  json.dump(file.read(), sys.stdout)
EOL
}

main() {
  minisign_secret="$default_minisign_secret"
  certs_dir="$default_certs_dir"
  output_file="$default_output_file"

  if ! command -v minisign > /dev/null 2>&1; then
    echo "minisign command not found"
    exit 1
  fi

  if ! command -v python3 > /dev/null 2>&1; then
    echo "python3 command not found"
    exit 1
  fi

  while [[ $# -gt 0 ]]; do
    case "$1" in
      -h|--help)
        usage
        exit 0
        ;;
      -c|--certs)
        certs_dir="$2"
        shift # consume arg
        ;;
      -s|--secret-key)
        minisign_secret="$2"
        shift # consume arg
        ;;
      -o|--output)
        output_file="$2"
        shift # consume arg
        ;;
      *)
        echo "Invalid argument $1"
        echo
        usage
        exit 1
        ;;
    esac

    shift # consume arg
  done

  if [[ ! -f "$minisign_secret" ]]; then
    echo "Minisign secret key doesn't exist"
    exit 1
  fi

  if [[ ! -d "$certs_dir" ]]; then
    echo "Certificate directory doesn't exist"
    exit 1
  fi

  temp_dir="$(mktemp -d)"
  generate-data "$certs_dir" "$temp_dir/fingerprints.txt"

  echo "Enter minisign secret key password"
  minisign -q -S -s "$minisign_secret" -m "$temp_dir/fingerprints.txt"

  cat > "$output_file" <<EOL
{
  "version": 1,
  "fingerprints": $(jsonify "$temp_dir/fingerprints.txt"),
  "signature": $(jsonify "$temp_dir/fingerprints.txt.minisig")
}
EOL

  rm -r "$temp_dir"
}

main "$@"
