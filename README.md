# CAliper

Make sure your domains are using HTTPS certificates that you actually own.

## What is this solving?

Browsers have many Certificate Authorities (CA) that they trust and any of them can generate a valid signature for any certificate for any domain. This will verify that the certificate your browser uses is the one you created and show an error page when that is not the case.

## Why is that an issue?

This is an issue if a CA signs a certificate for your domain without your knowledge. That can happen either maliciously or by accident, but in either case it means someone else can spoof your https certificate. That certificate can then be used to serve a malicious page on your domain that the browser thinks is valid, for example via a man-in-the-middle attack.

## How does it do that?

This is a browser extension that independently verifies certificate fingerprints with a web server and a pre-shared public key. This is essentially the same as what https certificate pinning does, but that is not available in current browsers.

## Specification

This uses [Minisign](https://jedisct1.github.io/minisign/) for signing data sent to the extension.

### Version 1

The data that is signed is a mapping of domain names to certificate fingerprints. Each line should have a comma separated list of domains followed by a single space and the certificate's sha256 fingerprint. The domains can include a star glob at the beginning ("*."). The fingerprint should be in the format of colon separated bytes that are uppercase hexidecimal.

Example data:

```
example.com,example.org 92:50:71:1C:54:DE:54:6F:43:70:E0:C3:D3:A3:EC:45:BC:96:09:2A:25:A4:A7:1A:1A:FA:39:6A:F7:04:7E:B8
www.example.com,www.example.org 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00
```

This data is signed and the data, signature, and version go into a JSON file that the extension downloads.

```json
{
  "version": 1,
  "fingerprints": "example.com,example.org 92:50:71:1C:54:DE:54:6F:43:70:E0:C3:D3:A3:EC:45:BC:96:09:2A:25:A4:A7:1A:1A:FA:39:6A:F7:04:7E:B8\nwww.example.com,www.example.org 00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00",
  "signature": "untrusted comment: signature from minisign secret key\nRWQ8CskH8w7WMkmXd+dsJ9p48Aplu7e1eaI1SbO5h5g5Mk9h6r9J+PAFM4wkac8HMbjPDZSNDQrynHvrc0+bbk5MmpKRExn36Qc=\ntrusted comment: timestamp:1588982282\tfile:fingerprints.txt\nyXkVK/fsXLfAG+hGI/LnJwbfUWSktnRnrrSEP4akGs6vIkUagJmGF2Lz6lS4KEzkHiBbc3CaZ8mDMDzH62NrAw=="
}

```


### Example Minisign keys

Public key:

```
untrusted comment: minisign public key 32D60EF307C90A3C
RWQ8CskH8w7WMgECXMnsilVcoc1x52Ddklamgmbc2kVnBR/rL9NjPJSK
```

Private key:

```
untrusted comment: minisign encrypted secret key
RWRTY0IyvNpA6loSfOQ8IJf+TkBkHIfV20H9Igab3zUOP8VkUr8AAAACAAAAAAAAAEAAAAAAIfnpm3i5LAdDE7dUpUCA8r14DlOpA9o6Zy+nqni0x9l6ZTZ8qVGumH/du7ZAN43iWXuP6Mc2Uda1fqazaOxJlWgjito2AymRz+OJyKOzyjvfDn95FkGwEr/OdsoVOUf7BlFhPOl5Ock=
```